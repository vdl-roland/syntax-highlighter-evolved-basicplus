=== SyntaxHighlighter Evolved: Open Insight Brush ===
Contributors: roland 
Tags: syntaxhighlighting
Requires at least: 2.7
Tested up to: 4.2.2
Stable tag: 1.5.0
License: dual - GPLv3 or MIT
License URI: http://www.gnu.org/licenses/gpl-3.0.txt

Adds support for the Basic+ language to the SyntaxHighlighter Evolved plugin.

== Installation ==

1. Make sure the Syntaxhighlighter Evolved plugin is installed.
1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0.0 =
* Initial Version based on https://github.com/jbratu/SyntaxHighlighterForOpenInsight
