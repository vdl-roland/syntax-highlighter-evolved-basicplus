<?php

/*
Plugin Name: SyntaxHighlighter Evolved: Basic Plus 
Description: Adds support for the Basic+ language to the SyntaxHighlighter Evolved plugin.
Version: 1.0.0
Author: roland
Author URI: https://github.com/jbratu/SyntaxHighlighterForOpenInsight
License: GPL3 or MIT
*/
 

// SyntaxHighlighter Evolved doesn't do anything until early in the "init" hook, so best to wait until after that
add_action( 'init', 'syntaxhighlighter_basicplus_regscript' );
 
// Tell SyntaxHighlighter Evolved about this new language/brush
add_filter( 'syntaxhighlighter_brushes', 'syntaxhighlighter_basicplus_addlang' );
 
// Register the brush file with WordPress
function syntaxhighlighter_basicplus_regscript() {
    wp_register_script( 'syntaxhighlighter-brush-basicplus', plugins_url( 'shBrushBasicPlus.js', __FILE__ ), array('syntaxhighlighter-core'), '1.2.4' );
}
 
// Filter SyntaxHighlighter Evolved's language array
function syntaxhighlighter_basicplus_addlang( $brushes ) {
    $brushes['basicplus'] = 'basicplus';
 
    return $brushes;
}
 
?>
